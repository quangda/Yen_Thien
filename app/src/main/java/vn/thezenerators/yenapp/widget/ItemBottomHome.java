package vn.thezenerators.yenapp.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import vn.thezenerators.yenapp.R;

public class ItemBottomHome extends LinearLayout {

    private TextView title;
    private ImageView icon;

    public ItemBottomHome(Context context) {
        super(context);
        init(context, null);
    }

    public ItemBottomHome(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ItemBottomHome(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_bottom_home, this);

        title = findViewById(R.id.title);
        icon = findViewById(R.id.ivIcon);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ItemBottomHome);
        initTitle(typedArray);
        initIcon(typedArray);
    }

    private void initTitle(TypedArray typedArray) {
        String titleApp = typedArray.getString(R.styleable.ItemBottomHome_title);
        title.setText(titleApp);
    }

    private void initIcon(TypedArray mTypedArray) {
        Drawable iconDrawable = mTypedArray.getDrawable(R.styleable.ItemBottomHome_icon);
        if (iconDrawable != null) {
            icon.setImageDrawable(iconDrawable);
        }
        icon.setVisibility(iconDrawable == null ? GONE : VISIBLE);
    }
}