package vn.thezenerators.yenapp.api

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import io.realm.Realm
import vn.thezenerators.yenapp.Constants
import vn.thezenerators.yenapp.model.User
import vn.thezenerators.yenapp.ui.login.ILoginNavigator
import vn.thezenerators.yenapp.utils.ToastUtil
import java.util.*

object LoginRequest {
    val TAG = "LoginRequest"

    class GoogleLoginRequest : GoogleApiClient.OnConnectionFailedListener {

        fun loginGoogle(fragmentActivity: FragmentActivity, mGoogleApiClient: GoogleApiClient) {
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            fragmentActivity.startActivityForResult(signInIntent, Constants.RequestCode.GoogleSignIn)
        }

        fun loginOnFirebase(data: Intent, navigator: ILoginNavigator) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (!result.isSuccess) {
                return
            }

            val realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            val user = realm.createObject(User::class.java)
            user.access_token = "123456"
            user.isLoggedIn = true
            realm.copyToRealm(user)
            realm.commitTransaction()

            navigator.loginSuccess()

            val account = result.signInAccount
            val credential = GoogleAuthProvider.getCredential(account!!.idToken, null)
            val mAuth = FirebaseAuth.getInstance()
            mAuth.signInWithCredential(credential)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful)
                        Log.d(TAG, "signInWithCredential:success")
                    else
                        Log.w(TAG, "signInWithCredential:failure", task.exception)
                }
        }

        override fun onConnectionFailed(connectionResult: ConnectionResult) {

        }
    }

    class FacebookLoginRequest {

        fun loginFB(activity: Activity, mCallbackManager: CallbackManager, navigator: ILoginNavigator) {
            LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile"))
            LoginManager.getInstance().registerCallback(mCallbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        val realm = Realm.getDefaultInstance()
                        realm.beginTransaction()
                        val user = realm.createObject(User::class.java)
                        user.access_token = "123456"
                        user.isLoggedIn = true
                        realm.copyToRealm(user)
                        realm.commitTransaction()

                        navigator.loginSuccess()

                        val credential = FacebookAuthProvider.getCredential(loginResult.accessToken.token)
                        val mAuth = FirebaseAuth.getInstance()
                        mAuth.signInWithCredential(credential)
                            .addOnCompleteListener(activity) { task ->
                                if (task.isSuccessful)
                                    Log.d(TAG, "signIn With Credential:success")
                                else
                                    Log.w(TAG, "signIn With Credential:failure", task.exception)
                            }
                    }

                    override fun onCancel() {

                    }

                    override fun onError(e: FacebookException) {
                        ToastUtil.shortToast(activity, e.toString())
                    }
                })
        }
    }
}