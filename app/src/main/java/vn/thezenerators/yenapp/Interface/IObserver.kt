package vn.thezenerators.yenapp.Interface

interface IObserver {
    fun destroyInstance()
}