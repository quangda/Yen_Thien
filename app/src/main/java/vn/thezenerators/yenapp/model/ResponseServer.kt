package vn.thezenerators.yenapp.model

open class ResponseServer {

    val error: Int = 0

    val code: Int = 0

    val message: String = ""
}