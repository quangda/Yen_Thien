package vn.thezenerators.yenapp.model

import io.realm.RealmObject

open class User : RealmObject() {

    var access_token: String = ""

    var isLoggedIn: Boolean = false
}