package vn.thezenerators.yenapp.base

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import vn.thezenerators.yenapp.R

abstract class BaseAdapter<T>(protected var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_LOAD = 0
    private val TYPE_DATA = 1

    protected abstract val resLayout: Int

    protected val resLoadMore: Int
        get() = R.layout.row_load

    protected val tagName: String
        get() = javaClass.name

    protected var isMoreDataAvailable = false
    protected var isLoading = false

    protected lateinit var loadMoreListener: OnLoadMoreListener

    protected fun addItem(position: Int) {
        Handler(Looper.getMainLooper()).post { notifyItemInserted(position) }
    }

    protected fun removeItem(position: Int) {
        Handler(Looper.getMainLooper()).post { notifyItemRemoved(position) }
    }

    protected fun changeItem(position: Int) {
        Handler(Looper.getMainLooper()).post { notifyItemChanged(position) }
    }

    protected fun changeRangeItem(positionStart: Int, itemCount: Int) {
        Handler(Looper.getMainLooper()).post { notifyItemRangeChanged(positionStart, itemCount) }
    }

    protected fun updateList() {
        Handler(Looper.getMainLooper()).post { notifyDataSetChanged() }
    }

    override fun getItemViewType(position: Int): Int {
        return TYPE_DATA
    }

    protected abstract fun getViewHolder(viewGroup: ViewGroup): BaseViewHolder

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_LOAD)
            LoadHolder(
                LayoutInflater.from(context)
                    .inflate(resLoadMore, viewGroup, false)
            )
        else
            getViewHolder(viewGroup)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        bindView(viewHolder, position)
    }

    protected abstract fun bindView(viewHolder: RecyclerView.ViewHolder, position: Int)

    interface OnLoadMoreListener {
        fun onLoadMore()
    }
}