package vn.thezenerators.yenapp

class Constants {

    object Url {
        val URL_ROOT = ""
    }

    object RequestCode {
        val GoogleSignIn = 100
    }
}