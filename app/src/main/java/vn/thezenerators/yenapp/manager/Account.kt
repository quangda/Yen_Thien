package vn.thezenerators.yenapp.manager

import vn.thezenerators.yenapp.Interface.IObserver
import vn.thezenerators.yenapp.model.ResponseServer
import vn.thezenerators.yenapp.model.User

class Account : ResponseServer(), IObserver {

    private object Holder {
        var INSTANCE: Account? = null
    }

    companion object {
        @JvmStatic
        fun getInstance(): Account {
            if (Holder.INSTANCE == null) {
                synchronized(Account::class.java) {
                    Holder.INSTANCE = Account()
                    Subject.INSTANCE.attach(Holder.INSTANCE!!)
                }
            }
            return Holder.INSTANCE!!
        }
    }

    override fun destroyInstance() {
        Holder.INSTANCE = null
    }

    var user: User? = null
}