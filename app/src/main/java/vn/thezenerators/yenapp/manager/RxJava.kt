package vn.thezenerators.yenapp.manager

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

enum class RxJava {

    INSTANCE;

    private var compositeDisposable: CompositeDisposable? = null

    fun addSubscribe(disposable: Disposable) {
        if (compositeDisposable == null)
            compositeDisposable = CompositeDisposable()
        compositeDisposable!!.add(disposable)
    }

    fun unsubscibe() {
        if (compositeDisposable != null)
            compositeDisposable!!.clear()
    }
}