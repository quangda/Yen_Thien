package vn.thezenerators.yenapp.manager

import vn.thezenerators.yenapp.Interface.IObserver

import java.util.ArrayList

enum class Subject {

    INSTANCE;

    private var observers: MutableList<IObserver>? = null
    var isCanDestroyed = true

    fun attach(iObserver: IObserver) {
        if (observers == null)
            observers = ArrayList()
        observers!!.add(iObserver)
    }

    fun destroyAllInstance() {
        if (!isCanDestroyed) {
            isCanDestroyed = true
            return
        }
        if (observers == null)
            return
        for (ob in observers!!)
            ob.destroyInstance()
        observers!!.clear()
    }
}