package vn.thezenerators.yenapp.ui.main

import android.content.Intent
import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseActivity
import vn.thezenerators.yenapp.base.BaseFragment
import vn.thezenerators.yenapp.databinding.ActivityMainBinding
import vn.thezenerators.yenapp.ui.main.explore.ExploreFragment
import vn.thezenerators.yenapp.ui.main.home.HomeFragment
import vn.thezenerators.yenapp.ui.main.meditate.MeditateFragment
import vn.thezenerators.yenapp.ui.main.music.MusicFragment
import javax.inject.Inject

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private var idTabCurrent: Int = 0

    @Inject
    private lateinit var mMainViewModel: MainViewModel
    private lateinit var mActivityMainBinding: ActivityMainBinding

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.activity_main

    override val viewModel: MainViewModel
        get() {
            mMainViewModel = MainViewModel()
            return mMainViewModel
        }

    override fun initData() {
        mActivityMainBinding = viewDataBinding

        /*val appBarConfiguration = AppBarConfiguration.Builder(
            R.id.navigation_home,
            R.id.navigation_meditate,
            R.id.navigation_music,
            R.id.navigation_explore
        ).build()*//*

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        //NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
        NavigationUI.setupWithNavController(nav_view, navController)*/

        initBottomTab()
    }

    fun initBottomTab() {
        replaceFragmentTab(HomeFragment(), R.id.ibhHome)

        ibhHome.findViewById<View>(R.id.layout_choose).setOnClickListener {
            replaceFragmentTab(HomeFragment(), R.id.ibhHome)
        }
        ibhMeditate.findViewById<View>(R.id.layout_choose).setOnClickListener {
            replaceFragmentTab(MeditateFragment(), R.id.ibhMeditate)
        }
        ibhMusic.findViewById<View>(R.id.layout_choose).setOnClickListener {
            replaceFragmentTab(MusicFragment(), R.id.ibhMusic)
        }
        ibhExplore.findViewById<View>(R.id.layout_choose).setOnClickListener {
            replaceFragmentTab(ExploreFragment(), R.id.ibhExplore)
        }
    }

    fun setColorActiveBottom(id: Int) {
        setActive(ibhHome, id == R.id.ibhHome)
        setActive(ibhMeditate, id == R.id.ibhMeditate)
        setActive(ibhMusic, id == R.id.ibhMusic)
        setActive(ibhExplore, id == R.id.ibhExplore)
    }

    fun setActive(view: View, active: Boolean) {
        val title = view.findViewById<TextView>(R.id.title)
        if (active) {
            title.setTypeface(title.typeface, Typeface.BOLD)
            //title.setTextColor(getColor(android.R.color.white))
        } else {
            title.setTypeface(null, Typeface.NORMAL)
            //title.setTextColor(getColor(android.R.color.darker_gray))
        }

        /*val icon = view.findViewById<ImageView>(R.id.ivIcon)
        if (active)
            icon.setColorFilter(getColor(android.R.color.white), PorterDuff.Mode.MULTIPLY)
        else
            icon.setColorFilter(getColor(android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY)*/
    }

    fun replaceFragmentTab(baseFragment: BaseFragment<*, *>, idTab: Int) {
        if (idTabCurrent == idTab)
            return
        idTabCurrent = idTab
        setColorActiveBottom(idTab)
        replaceFragment(baseFragment)
    }

    override fun getContainerId(): Int {
        return R.id.frame_container
    }

    override fun onActivity_Result(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    override fun onRequestPermissions_Result(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

    }
}