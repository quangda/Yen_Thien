package vn.thezenerators.yenapp.ui.main.home

import vn.thezenerators.yenapp.base.BaseViewModel
import vn.thezenerators.yenapp.ui.main.IMainNavigator

class HomeViewModel : BaseViewModel<IMainNavigator>()