package vn.thezenerators.yenapp.ui.login

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import com.facebook.CallbackManager
import com.google.android.gms.common.api.GoogleApiClient
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import vn.thezenerators.yenapp.api.LoginRequest
import vn.thezenerators.yenapp.base.BaseViewModel

class LoginViewModel : BaseViewModel<ILoginNavigator>() {

    fun onGoogleLoginClick(fragmentActivity: FragmentActivity, googleApiClient: GoogleApiClient) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Any> { emitter ->
                LoginRequest.GoogleLoginRequest().loginGoogle(fragmentActivity, googleApiClient)
                emitter.onComplete()
            }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->

                }, { throwable ->

                })
        )
    }

    fun loginOnFirebase(data: Intent) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Any> { emitter ->
                LoginRequest.GoogleLoginRequest().loginOnFirebase(data, getNavigator()!!)
                emitter.onComplete()
            }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->

                }, { throwable ->

                })
        )
    }

    fun onFbLoginClick(fragmentActivity: FragmentActivity, callbackManager: CallbackManager) {
        compositeDisposable.add(
            Observable.create(ObservableOnSubscribe<Any> { emitter ->
                LoginRequest.FacebookLoginRequest().loginFB(fragmentActivity, callbackManager, getNavigator()!!)
                emitter.onComplete()
            }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->

                }, { throwable ->

                })
        )
    }
}