package vn.thezenerators.yenapp.ui.splash

import io.realm.Realm
import vn.thezenerators.yenapp.base.BaseViewModel
import vn.thezenerators.yenapp.manager.Account
import vn.thezenerators.yenapp.model.User

class SplashViewModel : BaseViewModel<ISplashNavigator>() {

    fun decideNextActivity() {
        val realm = Realm.getDefaultInstance()
        val users = realm.where(User::class.java).findAll()
        for (user in users)
            if (user.isLoggedIn) {
                Account.getInstance().user = user
                getNavigator()!!.openMainActivity()
                return
            }

        getNavigator()!!.openLoginActivity()
    }
}