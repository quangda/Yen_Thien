package vn.thezenerators.yenapp.ui.splash

import android.content.Intent
import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseActivity
import vn.thezenerators.yenapp.databinding.ActivitySplashBinding
import vn.thezenerators.yenapp.ui.login.LoginActivity
import vn.thezenerators.yenapp.ui.main.MainActivity

import javax.inject.Inject

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(), ISplashNavigator {

    @Inject
    private lateinit var mSplashViewModel: SplashViewModel

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.activity_splash

    override val viewModel: SplashViewModel
        get() {
            mSplashViewModel = SplashViewModel()
            return mSplashViewModel
        }

    override fun initData() {
        mSplashViewModel.setNavigator(this)
        mSplashViewModel.decideNextActivity()
    }

    override fun onActivity_Result(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    override fun onRequestPermissions_Result(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

    }

    override fun openLoginActivity() {
        startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
        finishAffinity()
    }

    override fun openMainActivity() {
        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        finishAffinity()
    }
}