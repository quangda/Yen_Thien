package vn.thezenerators.yenapp.ui.login.forget_password

import android.app.AlertDialog
import dmax.dialog.SpotsDialog
import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseFragment
import vn.thezenerators.yenapp.databinding.FragmentForgetPasswordBinding

import javax.inject.Inject

class ForgetPasswordFragment : BaseFragment<FragmentForgetPasswordBinding, ForgetPasswordViewModel>() {

    private lateinit var progressDialog: AlertDialog

    @Inject
    private lateinit var mForgetViewModel: ForgetPasswordViewModel
    private lateinit var mForgetBinding: FragmentForgetPasswordBinding

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.fragment_forget_password

    override val viewModel: ForgetPasswordViewModel
        get() {
            mForgetViewModel = ForgetPasswordViewModel()
            return mForgetViewModel
        }

    override fun initData() {
        mForgetBinding = viewDataBinding
        progressDialog = SpotsDialog.Builder()
            .setContext(activity)
            .setMessage(R.string.loading)
            .build()
    }
}