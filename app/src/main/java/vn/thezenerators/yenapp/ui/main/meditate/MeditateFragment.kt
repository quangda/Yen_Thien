package vn.thezenerators.yenapp.ui.main.meditate

import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseFragment
import vn.thezenerators.yenapp.databinding.FragmentMeditateBindingImpl

import javax.inject.Inject

class MeditateFragment : BaseFragment<FragmentMeditateBindingImpl, MeditateViewModel>() {

    @Inject
    private lateinit var mThienDinhBinding: FragmentMeditateBindingImpl
    private lateinit var mThienDinhViewModel: MeditateViewModel

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.fragment_meditate

    override val viewModel: MeditateViewModel
        get() {
            mThienDinhViewModel = MeditateViewModel()
            return mThienDinhViewModel
        }

    override fun initData() {
        mThienDinhBinding = viewDataBinding
    }
}