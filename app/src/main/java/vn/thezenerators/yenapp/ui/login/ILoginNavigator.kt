package vn.thezenerators.yenapp.ui.login

interface ILoginNavigator {

    fun loginSuccess()

    fun showLoadingDialog()

    fun hideLoadingDialog()
}