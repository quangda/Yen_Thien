package vn.thezenerators.yenapp.ui.main.explore

import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseFragment
import vn.thezenerators.yenapp.databinding.FragmentExploreBinding

import javax.inject.Inject

class ExploreFragment : BaseFragment<FragmentExploreBinding, ExploreViewModel>() {

    @Inject
    private lateinit var mKhamPhaBinding: FragmentExploreBinding
    private lateinit var mKhamPhaViewModel: ExploreViewModel

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.fragment_explore

    override val viewModel: ExploreViewModel
        get() {
            mKhamPhaViewModel = ExploreViewModel()
            return mKhamPhaViewModel
        }

    override fun initData() {
        mKhamPhaBinding = viewDataBinding
    }
}