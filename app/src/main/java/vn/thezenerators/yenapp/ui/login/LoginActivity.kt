package vn.thezenerators.yenapp.ui.login

import android.app.AlertDialog
import android.content.Intent
import android.view.View
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import kotlinx.android.synthetic.main.activity_login.*
import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.Constants
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseActivity
import vn.thezenerators.yenapp.databinding.ActivityLoginBinding
import vn.thezenerators.yenapp.ui.login.forget_password.ForgetPasswordFragment
import vn.thezenerators.yenapp.ui.login.register.RegisterFragment
import vn.thezenerators.yenapp.ui.main.MainActivity
import vn.thezenerators.yenapp.utils.*
import javax.inject.Inject

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(), View.OnClickListener,
    GoogleApiClient.OnConnectionFailedListener, ILoginNavigator {

    lateinit var callbackManager: CallbackManager
    lateinit var googleApiClient: GoogleApiClient
    private lateinit var loadingDialog: AlertDialog

    @Inject
    private lateinit var mLoginViewModel: LoginViewModel
    private lateinit var mActivityLoginBinding: ActivityLoginBinding

    override val layoutId: Int
        get() = R.layout.activity_login

    override val bindingVariable: Int
        get() = BR.data

    override val viewModel: LoginViewModel
        get() {
            mLoginViewModel = LoginViewModel()
            return mLoginViewModel
        }

    override fun initData() {
        mActivityLoginBinding = viewDataBinding
        mLoginViewModel.setNavigator(this)

        img_fb.setOnTouchListener(
            OnTouchClickListener(
                OnClickListener(this),
                applicationContext
            )
        )
        img_gg.setOnTouchListener(
            OnTouchClickListener(
                OnClickListener(this),
                applicationContext
            )
        )
        lbl_register.setOnTouchListener(
            OnTouchClickListener(
                OnClickListener(this),
                applicationContext
            )
        )
        lbl_forget_password.setOnTouchListener(
            OnTouchClickListener(
                OnClickListener(this),
                applicationContext
            )
        )

        callbackManager = CallbackManager.Factory.create()
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by GoogleSignInOptions Instance(gso).
        googleApiClient = GoogleApiClient.Builder(applicationContext)
            .enableAutoManage(this@LoginActivity, this@LoginActivity)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()

        loadingDialog = DialogUtil.initSpotDialog(this@LoginActivity, getString(R.string.loading))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.img_fb -> mLoginViewModel.onFbLoginClick(this@LoginActivity, callbackManager)

            R.id.img_gg -> mLoginViewModel.onGoogleLoginClick(this@LoginActivity, googleApiClient)

            R.id.lbl_register -> TransactionUtil.addFragment(supportFragmentManager, RegisterFragment())

            R.id.lbl_forget_password -> TransactionUtil.addFragment(supportFragmentManager, ForgetPasswordFragment())
        }
    }

    override fun onActivity_Result(requestCode: Int, resultCode: Int, data: Intent?) {

    }

    override fun onRequestPermissions_Result(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.GoogleSignIn) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result.isSuccess) {
                // Google Sign In was successful, authenticate with Firebase
                mLoginViewModel.loginOnFirebase(data!!)
            } else {
                // Google Sign In failed, update UI appropriately
                ToastUtil.shortToast(this, "Google Sign In failed")
            }
        } else {
            // Pass the activity result back to the Facebook SDK
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun loginSuccess() {
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun showLoadingDialog() {
        loadingDialog.show()
    }

    override fun hideLoadingDialog() {
        loadingDialog.hide()
    }
}