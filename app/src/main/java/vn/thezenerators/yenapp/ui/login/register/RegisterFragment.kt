package vn.thezenerators.yenapp.ui.login.register

import android.view.View
import com.facebook.FacebookSdk.getApplicationContext
import kotlinx.android.synthetic.main.fragment_register.*
import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseFragment
import vn.thezenerators.yenapp.databinding.FragmentRegisterBinding
import vn.thezenerators.yenapp.ui.login.LoginActivity
import vn.thezenerators.yenapp.ui.login.LoginViewModel
import vn.thezenerators.yenapp.utils.OnClickListener
import vn.thezenerators.yenapp.utils.OnTouchClickListener
import javax.inject.Inject

class RegisterFragment : BaseFragment<FragmentRegisterBinding, LoginViewModel>(), View.OnClickListener {

    @Inject
    private lateinit var mLoginViewModel: LoginViewModel
    private lateinit var mFragmentRegisterBinding: FragmentRegisterBinding

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.fragment_register

    override val viewModel: LoginViewModel
        get() {
            mLoginViewModel = LoginViewModel()
            return mLoginViewModel
        }

    override fun initData() {
        mFragmentRegisterBinding = viewDataBinding

        img_fb.setOnTouchListener(
            OnTouchClickListener(
                OnClickListener(this),
                getApplicationContext()
            )
        )
        img_gg.setOnTouchListener(
            OnTouchClickListener(
                OnClickListener(this),
                getApplicationContext()
            )
        )
        lbl_login.setOnTouchListener(
            OnTouchClickListener(
                OnClickListener(this),
                getApplicationContext()
            )
        )
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.img_fb -> mLoginViewModel.onFbLoginClick(activity!!, (activity as LoginActivity).callbackManager)

            R.id.img_gg -> mLoginViewModel.onGoogleLoginClick(
                activity!!,
                (activity as LoginActivity).googleApiClient
            )

            R.id.lbl_login -> activity?.supportFragmentManager?.popBackStack()
        }
    }
}