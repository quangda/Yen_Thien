package vn.thezenerators.yenapp.ui.main.music

import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseFragment
import vn.thezenerators.yenapp.databinding.FragmentMusicBinding
import javax.inject.Inject

class MusicFragment : BaseFragment<FragmentMusicBinding, MusicViewModel>() {

    @Inject
    private lateinit var mAmNhacBinding: FragmentMusicBinding
    private lateinit var mAmNhacViewModel: MusicViewModel

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.fragment_music

    override val viewModel: MusicViewModel
        get() {
            mAmNhacViewModel = MusicViewModel()
            return mAmNhacViewModel
        }

    override fun initData() {
        mAmNhacBinding = viewDataBinding
    }
}