package vn.thezenerators.yenapp.ui.main.home

import vn.thezenerators.yenapp.BR
import vn.thezenerators.yenapp.R
import vn.thezenerators.yenapp.base.BaseFragment
import vn.thezenerators.yenapp.databinding.FragmentHomeBinding

import javax.inject.Inject

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    @Inject
    private lateinit var mTrangChuBinding: FragmentHomeBinding
    private lateinit var mTrangChuViewModel: HomeViewModel

    override val bindingVariable: Int
        get() = BR.data

    override val layoutId: Int
        get() = R.layout.fragment_home

    override val viewModel: HomeViewModel
        get() {
            mTrangChuViewModel = HomeViewModel()
            return mTrangChuViewModel
        }

    override fun initData() {
        mTrangChuBinding = viewDataBinding
    }
}