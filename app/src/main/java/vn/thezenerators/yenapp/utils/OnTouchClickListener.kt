package vn.thezenerators.yenapp.utils

import android.content.Context
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import vn.thezenerators.yenapp.R

/**
 * Created by Manh Dang on 03/05/2018.
 */

class OnTouchClickListener(private val mListener: View.OnClickListener, private val context: Context) :
    View.OnTouchListener {

    /**
     * min movement to detect as a click action.
     */
    private val minMove = 20
    private var startX: Float = 0.toFloat()
    private var startY: Float = 0.toFloat()

    private fun isAClick(startX: Float, endX: Float, startY: Float, endY: Float): Boolean {
        val differenceX = Math.abs(startX - endX)
        val differenceY = Math.abs(startY - endY)
        return !(differenceX > minMove || differenceY > minMove)
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        val zoom_in = AnimationUtils.loadAnimation(context, R.anim.zoom_in)
        val zoom_out = AnimationUtils.loadAnimation(context, R.anim.zoom_out)

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
                v.startAnimation(zoom_out)
                return true
            }

            MotionEvent.ACTION_UP -> {
                val endX = event.x
                val endY = event.y
                v.startAnimation(zoom_in)
                if (isAClick(startX, endX, startY, endY)) {
                    Handler().postDelayed({ mListener.onClick(v) }, 50)
                }
            }

            MotionEvent.ACTION_CANCEL -> v.startAnimation(zoom_in)
        }

        return false
    }
}