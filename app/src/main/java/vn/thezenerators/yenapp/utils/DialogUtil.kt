package vn.thezenerators.yenapp.utils

import android.app.Activity
import android.app.AlertDialog
import dmax.dialog.SpotsDialog

object DialogUtil {

    @JvmStatic
    fun initSpotDialog(activity: Activity, message: String): AlertDialog {
        return SpotsDialog.Builder()
            .setContext(activity)
            .setMessage(message)
            .build()
    }
}