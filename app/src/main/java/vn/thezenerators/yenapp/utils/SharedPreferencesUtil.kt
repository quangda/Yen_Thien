package vn.thezenerators.yenapp.utils

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object SharedPreferencesUtil {

    @JvmStatic
    fun getPrefferBool(context: Context, key: String, defaultValue: Boolean): Boolean {
        try {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return defaultValue
    }

    @JvmStatic
    fun putPrefferBool(context: Context, key: String, defaultValue: Boolean) {
        try {
            val sharedata = PreferenceManager.getDefaultSharedPreferences(context).edit()
            sharedata.putBoolean(key, defaultValue)
            sharedata.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun getPrefferInt(context: Context, key: String, defaultValue: Int): Int {
        try {
            return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return defaultValue
    }

    @JvmStatic
    fun putPrefferInt(context: Context, key: String, defaultValue: Int) {
        try {
            val sharedata = PreferenceManager.getDefaultSharedPreferences(context).edit()
            sharedata.putInt(key, defaultValue)
            sharedata.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun getPrefferLong(context: Context, key: String, defaultValue: Long): Long {
        try {
            return PreferenceManager.getDefaultSharedPreferences(context).getLong(key, defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return defaultValue
    }

    @JvmStatic
    fun putPrefferLong(context: Context, key: String, defaultValue: Long) {
        try {
            val sharedata = PreferenceManager.getDefaultSharedPreferences(context).edit()
            sharedata.putLong(key, defaultValue)
            sharedata.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun getPrefferString(context: Context, key: String, defaultValue: String): String? {
        try {
            return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return defaultValue
    }

    @JvmStatic
    fun putPrefferString(context: Context, key: String, defaultValue: String) {
        try {
            val sharedata = PreferenceManager.getDefaultSharedPreferences(context).edit()
            sharedata.putString(key, defaultValue)
            sharedata.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun <T> put(context: Context, key: String, defaultValue: T) {
        try {
            val type = object : TypeToken<T>() {

            }.type
            val json = Gson().toJson(defaultValue, type)
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(key, json)
            editor.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    operator fun <T> get(context: Context, key: String, type: Class<T>): T? {
        try {
            val json = PreferenceManager.getDefaultSharedPreferences(context).getString(key, "")
            return Gson().fromJson(json, type)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
}