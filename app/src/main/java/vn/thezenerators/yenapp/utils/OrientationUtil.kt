package vn.thezenerators.yenapp.utils

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Build
import android.view.Surface
import android.view.WindowManager

/*  * This class is used to lock orientation of android app in nay android devices
 */

object OrientationUtil {

    /**
     * Locks the device window in landscape mode.
     */
    @JvmStatic
    fun lockOrientationLandscape(activity: Activity) {
        try {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Locks the device window in portrait mode.
     */
    @JvmStatic
    fun lockOrientationPortrait(activity: Activity) {
        try {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Locks the device window in actual screen mode.
     */
    @JvmStatic
    fun lockOrientation(activity: Activity) {
        try {
            val orientation = activity.resources.configuration.orientation
            val rotation = (activity.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
                .rotation

            // Copied from Android docs, since we don't have these values in Froyo
            // 2.2
            var SCREEN_ORIENTATION_REVERSE_LANDSCAPE = 8
            var SCREEN_ORIENTATION_REVERSE_PORTRAIT = 9

            // Build.VERSION.SDK_INT <= Build.VERSION_CODES.FROYO
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
                SCREEN_ORIENTATION_REVERSE_LANDSCAPE = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                SCREEN_ORIENTATION_REVERSE_PORTRAIT = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }

            if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90) {
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                }
            } else if (rotation == Surface.ROTATION_180 || rotation == Surface.ROTATION_270) {
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Unlocks the device window in user defined screen mode.
     */
    @JvmStatic
    fun unlockOrientation(activity: Activity) {
        try {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}