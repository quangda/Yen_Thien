package vn.thezenerators.yenapp.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

object TransactionUtil {

    @JvmStatic
    fun addFragment(manager: FragmentManager, fragment: Fragment) {
        val transaction = manager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        /*.setCustomAnimations(
            R.anim.enter_from_right, R.anim.exit_to_left,
            R.anim.enter_from_left, R.anim.exit_to_right
        )*/
        transaction.add(android.R.id.content, fragment)
        transaction.addToBackStack(fragment::class.java.simpleName)
        transaction.commitAllowingStateLoss()
    }
}