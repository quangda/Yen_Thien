package vn.thezenerators.yenapp.utils

import android.content.Context
import android.os.Build.VERSION

object ColorUtil {

    @JvmStatic
    fun getColor(context: Context, i: Int): Int {
        try {
            return if (VERSION.SDK_INT >= 23) {
                context.resources.getColor(i, context.theme)
            } else context.resources.getColor(i)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return 0
    }
}