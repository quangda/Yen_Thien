package vn.thezenerators.yenapp.utils

import android.content.Context
import android.text.TextUtils
import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import java.util.concurrent.TimeUnit

object CountryCodeUtil {

    @JvmStatic
    fun setCountryCode(context: Context, code: String, version: String, packg: String) {
        try {
            val TAG = "setCountryCode()"
            val country_code = CountryCodeUtil.getCountryCode(context)

            if (TextUtils.isEmpty(country_code)) {
                val client = OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .build()

                object : Thread() {
                    override fun run() {
                        try {
                            val request = Request.Builder()
                                .url("https://ipinfo.io/json").get().build()

                            val response = client.newCall(request).execute()
                            val result = response.body()!!.string()
                            Log.i(TAG, "result = $result")
                            val jsonObject = JSONObject(result)
                            var country = jsonObject.getString("country").toUpperCase()

                            /*if (TextUtils.isEmpty(country))
                            country = Locale.getDefault().getCountry().toUpperCase();*/

                            if (country.contains("_")) {
                                val words = country.split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                if (words.size > 1)
                                    country = words[1]
                            }

                            SharedPreferencesUtil.putPrefferString(context, "country_code", country)
                            Log.i(TAG, "country = $country")

                        } catch (e: Exception) {
                            Log.i(TAG, "catch: " + e.message)
                            e.printStackTrace()
                        }

                    }
                }.start()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun getCountryCode(context: Context): String? {
        var country_code: String? = ""
        try {
            country_code = SharedPreferencesUtil.getPrefferString(context, "country_code", "")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return country_code
    }
}