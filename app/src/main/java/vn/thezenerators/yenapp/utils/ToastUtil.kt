package vn.thezenerators.yenapp.utils

import android.content.Context
import android.widget.Toast
import vn.thezenerators.yenapp.R

object ToastUtil {

    @JvmStatic
    fun shortToast(context: Context, text: String) {
        try {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun longToast(context: Context, text: String) {
        try {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun toastNoNetwork(context: Context) {
        try {
            shortToast(context, context.getString(R.string.no_internet))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}