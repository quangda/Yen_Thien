package vn.thezenerators.yenapp.utils

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.google.gson.reflect.TypeToken

import java.lang.reflect.Type

class ListSerializier : JsonSerializer<List<Any>>, JsonDeserializer<List<Any>> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): List<Any>? {
        try {
            val gson = Gson()
            val collectionType = object : TypeToken<List<Any>>() {

            }.type
            return gson.fromJson<List<Any>>(json, collectionType)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    override fun serialize(src: List<Any>, typeOfSrc: Type, context: JsonSerializationContext): JsonElement? {
        return null
    }
}