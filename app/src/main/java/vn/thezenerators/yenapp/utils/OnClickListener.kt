package vn.thezenerators.yenapp.utils

import android.os.Handler
import android.os.Looper
import android.view.View

/**
 * Created by Manh Dang on 03/05/2018.
 */

class OnClickListener(private val mListener: View.OnClickListener) : View.OnClickListener {

    private var isDoubleClick = false

    override fun onClick(v: View) {
        if (isDoubleClick) {
            return
        }

        isDoubleClick = true
        mListener.onClick(v)

        Handler(Looper.getMainLooper()).postDelayed({ isDoubleClick = false }, 1000)
    }
}