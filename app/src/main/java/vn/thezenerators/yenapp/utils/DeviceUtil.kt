package vn.thezenerators.yenapp.utils

import android.content.Context
import android.os.Build
import android.provider.Settings

object DeviceUtil {

    @JvmStatic
    fun getDeviceName(): String {
        var deviceName = ""
        try {
            val manufacturer = Build.MANUFACTURER
            val model = Build.MODEL
            if (model.startsWith(manufacturer)) {
                return capitalize(model)
            }
            if (manufacturer.equals("HTC", ignoreCase = true)) {
                return "HTC_$model"
            }
            deviceName = capitalize(manufacturer) + "_" + model
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return deviceName
    }

    @JvmStatic
    fun getDeviceOS(): String {
        try {
            return Build.VERSION.RELEASE
        } catch (e: Exception) {
            return "Unknown"
        }
    }

    @JvmStatic
    fun getDeviceId(context: Context): String {
        try {
            return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        } catch (e: Exception) {
            return "Unknown"
        }
    }

    @JvmStatic
    fun capitalize(str: String): String {
        var phrase = ""
        try {
            val arr = str.toCharArray()
            var capitalizeNext = true
            for (c in arr) {
                if (capitalizeNext && Character.isLetter(c)) {
                    phrase += Character.toUpperCase(c)
                    capitalizeNext = false
                    continue
                } else if (Character.isWhitespace(c)) {
                    capitalizeNext = true
                }
                phrase += c
            }
        } catch (e: Exception) {
            return "Unknown"
        }

        return phrase
    }
}