package vn.thezenerators.yenapp.utils

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import vn.thezenerators.yenapp.Constants
import java.util.concurrent.TimeUnit

object RetrofitInitiator {

    @JvmStatic
    fun <S> createService(serviceClass: Class<S>): S {
        return createService(serviceClass, Constants.Url.URL_ROOT)
    }

    @JvmStatic
    fun <S> createService(serviceClass: Class<S>, baseUrl: String): S {
        return createService(serviceClass, baseUrl, null)
    }

    @JvmStatic
    fun <S> createService(serviceClass: Class<S>, gson: Gson): S {
        return createService(serviceClass, Constants.Url.URL_ROOT, gson)
    }

    @JvmStatic
    fun <S> createService(serviceClass: Class<S>, baseUrl: String, gson: Gson?): S {
        val httpClient = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(httpClient)

        if (gson != null)
            builder.addConverterFactory(GsonConverterFactory.create(gson))
        else
            builder.addConverterFactory(GsonConverterFactory.create())

        return builder.build().create(serviceClass)
    }
}