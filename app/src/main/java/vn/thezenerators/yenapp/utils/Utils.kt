package vn.thezenerators.yenapp.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import org.json.JSONArray
import org.json.JSONObject
import vn.thezenerators.yenapp.R
import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    @JvmStatic
    fun hideKeyboard(context: Context) {
        try {
            val activity = context as Activity
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun showKeyboard(context: Context, editText: EditText) {
        try {
            editText.requestFocus()
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun showKeyboard(editText: EditText, dialog: Dialog) {
        try {
            editText.requestFocus()
            dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun splitOneDigitAfterDot(arg: String): String {
        var result = ""

        var countDot = 0
        var isMetDot = false

        for (i in 0 until arg.length) {
            if (isMetDot)
                countDot++
            if (arg[i] == '.')
                isMetDot = true

            if (countDot < 2)
                result += arg[i]
            else
                break
        }

        return result
    }

    @JvmStatic
    fun splitDot(number: Int): String {
        val test = number.toString() + ""
        return splitDot(test)
    }

    @JvmStatic
    fun splitDot(number: String): String {
        val a = StringBuilder()
        var k = 0

        for (i in number.length - 1 downTo 0) {
            k++
            a.append(number[i])
            if (k % 3 == 0 && i != 0)
                a.append(".")
        }
        return a.reverse().toString()
    }

    @JvmStatic
    fun getPhotoInDirectory(path: String): List<File>? {
        var allFiles: List<File>? = null
        val folder = File(path)
        if (folder.exists()) {
            try {
                allFiles = LinkedList(Arrays.asList(*folder.listFiles { dir, name ->
                    (name.endsWith(".JPG")
                            || name.endsWith(".JPEG")
                            || name.endsWith(".PNG")
                            || name.endsWith(".jpg")
                            || name.endsWith(".jpeg")
                            || name.endsWith(".png"))
                }!!))

            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("getPhotoInDirectory()", "Exception e: " + e.message)
            }

        }
        return allFiles
    }

    @JvmStatic
    fun convertDpToPixel(dp: Float, context: Context): Int {
        try {
            val resources = context.resources
            val metrics = resources.displayMetrics
            val px = dp * metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT
            return px.toInt()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return -1
    }

    @JvmStatic
    fun convertPixelToDP(px: Float, context: Context): Int {
        try {
            val resources = context.resources
            val metrics = resources.displayMetrics
            val dp = px * DisplayMetrics.DENSITY_DEFAULT / metrics.densityDpi
            return dp.toInt()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return -1
    }

    @JvmStatic
    fun jsonToMap(json: JSONObject): Map<String, Any> {
        var retMap: Map<String, Any> = HashMap()
        try {
            if (json !== JSONObject.NULL) {
                retMap = toMap(json)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return retMap
    }

    @JvmStatic
    fun toMap(`object`: JSONObject): Map<String, Any> {
        val map = HashMap<String, Any>()
        try {
            val keysItr = `object`.keys()
            while (keysItr.hasNext()) {
                val key = keysItr.next()
                var value = `object`.get(key)

                if (value is JSONArray) {
                    value = toList(value)
                } else if (value is JSONObject) {
                    value = toMap(value)
                }
                map[key] = value
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return map
    }

    @JvmStatic
    fun toList(array: JSONArray): List<Any> {
        val list = ArrayList<Any>()
        try {
            for (i in 0 until array.length()) {
                var value = array.get(i)
                if (value is JSONArray) {
                    value = toList(value)
                } else if (value is JSONObject) {
                    value = toMap(value)
                }
                list.add(value)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return list
    }

    @JvmStatic
    fun rand(min: Int, max: Int): Int {
        try {
            val rn = Random()
            val range = max - min + 1
            return min + rn.nextInt(range)
        } catch (e: Exception) {
            e.printStackTrace()
            return -1
        }

    }

    @JvmStatic
    fun addPhotoToGallery(context: Context, str: String) {
        try {
            val intent = Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE")
            intent.data = Uri.fromFile(File(str))
            context.sendBroadcast(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @JvmStatic
    fun convertTime(context: Context, time: String, dateFormat: String): String {
        val convert = ""
        try {
            val df = SimpleDateFormat(dateFormat)

            val date = df.parse(time)
            val previous = date!!.time
            val now = System.currentTimeMillis()

            var duration = now - previous
            duration = duration / 1000
            return if (duration < 60)
                duration.toString() + " " + context.getString(R.string.seconds_ago)
            else if (duration < 60 * 60)
                (duration / 60).toString() + " " + context.getString(R.string.minutes_ago)
            else if (duration < 60 * 60 * 24)
                (duration / 60 / 60).toString() + " " + context.getString(R.string.hours_ago)
            else
                (duration / 60 / 60 / 24).toString() + " " + context.getString(R.string.days_ago)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return convert
    }

    @JvmStatic
    fun getDateFromCalendar(calendar: Calendar): Date {
        return calendar.time
    }

    @JvmStatic
    fun getCalendarFromDate(date: Date): Calendar? {
        try {
            val c = Calendar.getInstance()
            c.time = date
            return c
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    @JvmStatic
    fun getFormatedDate(calendar: Calendar, dateFormat: String): String {
        try {
            return getFormatedDate(getDateFromCalendar(calendar), dateFormat)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    @JvmStatic
    fun getFormatedDate(date: Date, dateFormat: String): String {
        try {
            val format = SimpleDateFormat(dateFormat)
            return format.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    @JvmStatic
    fun getYesterday(dateFormat: String): String {
        try {
            val format = SimpleDateFormat(dateFormat)
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -1)
            return format.format(cal.time)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    @JvmStatic
    fun get7DaysAgo(dateFormat: String): String {
        try {
            val format = SimpleDateFormat(dateFormat)
            val cal = Calendar.getInstance()
            cal.add(Calendar.WEEK_OF_MONTH, -1)
            return format.format(cal.time)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    @JvmStatic
    fun get30DaysAgo(dateFormat: String): String {
        try {
            val format = SimpleDateFormat(dateFormat)
            val cal = Calendar.getInstance()
            cal.add(Calendar.MONTH, -1)
            return format.format(cal.time)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    @JvmStatic
    fun getLastMonth(dateFormat: String): Time {
        try {
            val format = SimpleDateFormat(dateFormat)
            val cal = Calendar.getInstance()
            cal.add(Calendar.MONTH, -1)
            val lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
            cal.set(Calendar.DAY_OF_MONTH, 1)
            val startDay = format.format(cal.time)
            cal.set(Calendar.DAY_OF_MONTH, lastDayOfMonth)
            val endDay = format.format(cal.time)
            return Time(startDay, endDay)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return Time("", "")
    }

    class Time(val startDay: String, val endDay: String)

    @JvmStatic
    fun showErrorDialog(context: Context, message: String) {
        try {
            AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.error))
                .setMessage(message)
                .setPositiveButton(R.string.close) { dialog, which ->
                    try {

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }.create().show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Load UTF8withBOM or any ansi text file.
     * @param filename which to be converted to string
     * @return String value of File
     */
    fun loadFileAsString(filename: String): String {
        try {
            val BUFLEN = 1024
            val s = BufferedInputStream(FileInputStream(filename), BUFLEN)
            val baos = ByteArrayOutputStream(BUFLEN)
            val bytes = ByteArray(BUFLEN)
            var isUTF8 = false
            var read: Int = 0
            var count = 0
            while (read != -1) {
                if (count == 0 && bytes[0] == 0xEF.toByte() && bytes[1] == 0xBB.toByte() && bytes[2] == 0xBF.toByte()) {
                    isUTF8 = true
                    baos.write(bytes, 3, read - 3) // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read)
                }
                count += read
                read = s.read(bytes)
            }
            s.close()
            return if (isUTF8) String(baos.toByteArray()) else String(baos.toByteArray())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }
}