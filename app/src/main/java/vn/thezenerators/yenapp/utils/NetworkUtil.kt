package vn.thezenerators.yenapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.text.format.Formatter
import android.util.Log
import java.net.Inet4Address
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*

object NetworkUtil {

    @JvmStatic
    fun isNetworkAvailable(context: Context): Boolean {
        try {
            val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = manager.activeNetworkInfo
            if (!info!!.isAvailable) {
                ToastUtil.toastNoNetwork(context)
                return false
            }

            if (!info.isConnected) {
                ToastUtil.toastNoNetwork(context)
                return false
            }
            return true

        } catch (e: Exception) {
            ToastUtil.toastNoNetwork(context)
            return false
        }
    }

    @JvmStatic
    fun setMobileDataEnabled(context: Context, enabled: Boolean) {
        try {
            val coman = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val iConnectivityManagerField = Class.forName(coman.javaClass.name).getDeclaredField("mService")
            iConnectivityManagerField.isAccessible = true
            val iConnectivityManager = iConnectivityManagerField.get(coman)

            val setMobileDataEnabledMethod = Class.forName(iConnectivityManager.javaClass.name)
                .getDeclaredMethod("setMobileDataEnabled", *arrayOf<Class<*>>(java.lang.Boolean.TYPE))
            setMobileDataEnabledMethod.isAccessible = true
            setMobileDataEnabledMethod.invoke(iConnectivityManager, *arrayOf<Any>(java.lang.Boolean.valueOf(enabled)))

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun isMobileDataEnabled(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        try {
            val m = Class.forName(cm.javaClass.name).getDeclaredMethod("getMobileDataEnabled", *arrayOfNulls(0))
            m.isAccessible = true
            return java.lang.Boolean.valueOf((m.invoke(cm, *arrayOfNulls(0)) as Boolean))
        } catch (e: Exception) {
            e.printStackTrace()
            return java.lang.Boolean.valueOf(false)
        }
    }

    /**
     * Convert byte array to hex string
     *
     * @param bytes toConvert
     * @return hexValue
     */
    /*public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }*/

    /**
     * Get utf8 byte array.
     *
     * @param str which to be converted
     * @return array of NULL if error was found
     */
    @JvmStatic
    fun getUTF8Bytes(str: String): ByteArray? {
        try {
            return str.toByteArray(charset("UTF-8"))
        } catch (ex: Exception) {
            return null
        }
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    @JvmStatic
    fun getMACAddress(interfaceName: String?): String {
        try {
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                if (interfaceName != null) {
                    if (!intf.name.equals(interfaceName, ignoreCase = true))
                        continue
                }

                val mac = intf.hardwareAddress ?: return ""
                val buf = StringBuilder()
                for (aMac in mac) buf.append(String.format("%02X:", aMac))
                if (buf.length > 0) buf.deleteCharAt(buf.length - 1)
                return buf.toString()
            }
        } catch (ignored: Exception) {
        }
        // for now eat exceptions
        return ""
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    @JvmStatic
    fun getIPAddress(useIPv4: Boolean): String {
        try {
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs = Collections.list(intf.inetAddresses)
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress) {
                        val sAddr = addr.hostAddress
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        val isIPv4 = sAddr.indexOf(':') < 0

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr
                        } else {
                            if (!isIPv4) {
                                val delim = sAddr.indexOf('%') // drop ip6 zone suffix
                                return if (delim < 0) sAddr.toUpperCase() else sAddr.substring(0, delim).toUpperCase()
                            }
                        }
                    }
                }
            }
        } catch (ignored: Exception) {
        }
        // for now eat exceptions
        return ""
    }

    @JvmStatic
    fun getLocalIpAddress(): String? {
        try {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intf = en.nextElement()
                val enumIpAddr = intf.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                        return inetAddress.getHostAddress()
                    }
                }
            }
        } catch (ex: SocketException) {
            ex.printStackTrace()
        }

        return null
    }

    @JvmStatic
    fun getDeviceipMobileData(): String {
        try {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val networkinterface = en.nextElement()
                val enumIpAddr = networkinterface.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                        return inetAddress.getHostAddress()
                    }
                }
            }
        } catch (ex: Exception) {
            Log.e("Current IP", ex.toString())
        }

        return ""
    }

    @JvmStatic
    fun getDeviceipWiFiData(context: Context): String {
        val wm = context.getSystemService(Context.WIFI_SERVICE) as android.net.wifi.WifiManager
        return Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
    }
}