# Info project

  - Tên: Yên App
  - Support MinimumOS: Android 6.0 (SDK 23)
  - TargetOS: Android 10.0 (SDK 29)
  - Language: Kotlin 1.3.50
  - Architecture: MVVM (use DataBinding) & Dependency Injection: Using Dagger2
  - CI/CD:
```java
image: openjdk:8-jdk

variables:
  ANDROID_COMPILE_SDK: "29"
  ANDROID_BUILD_TOOLS: "29.0.2"
  ANDROID_SDK_TOOLS:   "4333796"
```